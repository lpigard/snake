#include <time.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

void draw_circle(SDL_Renderer *renderer, int x, int y, int radius, SDL_Color color)
{
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    for (int w = 0; w < radius * 2; w++)
    {
        for (int h = 0; h < radius * 2; h++)
        {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if(dx*dx + dy*dy <= radius * radius)
            {
                SDL_RenderDrawPoint(renderer, x + dx, y + dy);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    // returns zero on success else non-zero
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
    }

    srand(time(NULL));   // Initialization, should only be called once.

    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    int Width = DM.w;
    int Height = DM.h;

    const int N0 = 60;
    const int NX = (Width / N0) * N0;
    const int NY = (Height / N0) * N0;
    const int FPS = 10;
    int dir = 0; // direction of snake

    SDL_Window* win = SDL_CreateWindow("SNAKE", // creates a window
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    NX, NY, 0);

    SDL_SetWindowFullscreen(win,
                            SDL_WINDOW_FULLSCREEN);

    // triggers the program that controls
    // your graphics hardware and sets flags
    Uint32 render_flags = SDL_RENDERER_ACCELERATED;

    // creates a renderer to render our images
    SDL_Renderer* rend = SDL_CreateRenderer(win, -1, render_flags);

    // creates a surface to load an image into the main memory
    SDL_Surface* surface;

    // please provide a path for your image
    surface = IMG_Load("snake.JPG");

    // loads image to our graphics hardware memory.
    SDL_Texture* tex = SDL_CreateTextureFromSurface(rend, surface);

    // clears main-memory
    SDL_FreeSurface(surface);

    // let us control our image position
    // so that we can move it with our keyboard.
    int snake_length = 1;
    SDL_Rect * snake = malloc(snake_length * sizeof *snake);

    // connects our texture with dest to control position
    SDL_QueryTexture(tex, NULL, NULL, &snake[0].w, &snake[0].h);
    snake[0].x = 0;
    snake[0].y = 0;
    snake[0].w = N0;
    snake[0].h = N0;

    int food_length = 0;
    SDL_Rect * food = malloc(food_length * sizeof *food);

    // controls animation loop
    int close = 0;

    // how often food spawns
    int period = FPS * 1;

    int time = 0;
    int last_dir = dir;

    // animation loop
    while (!close) {
        SDL_Event event;

        // Events management
        while (SDL_PollEvent(&event)) {
            switch (event.type) {

                case SDL_QUIT:
                // handling of close button
                close = 1;
                break;

                case SDL_KEYDOWN:
                // keyboard API for key pressed
                switch (event.key.keysym.scancode) {
                    case SDL_SCANCODE_W:
                    case SDL_SCANCODE_UP:
                    if(last_dir != 3)
                    {
                        dir = 1;
                    }
                    break;
                    case SDL_SCANCODE_A:
                    case SDL_SCANCODE_LEFT:
                    if(last_dir != 0)
                    {
                        dir = 2;
                    }
                    break;
                    case SDL_SCANCODE_S:
                    case SDL_SCANCODE_DOWN:
                    if(last_dir != 1)
                    {
                        dir = 3;
                    }
                    break;
                    case SDL_SCANCODE_D:
                    case SDL_SCANCODE_RIGHT:
                    if(last_dir != 2)
                    {
                        dir = 0;
                    }
                    break;
                    default:
                    break;
                }
            }
        }

        const SDL_Rect last = snake[snake_length - 1];

        for(int i = snake_length - 1; i > 0; i--)
        {
            snake[i] = snake[i - 1];
        }

        switch(dir)
        {
            case 0: snake[0].x = (snake[0].x + N0) % NX; break;
            case 1: snake[0].y = (snake[0].y - N0 + NY) % NY; break;
            case 2: snake[0].x = (snake[0].x - N0 + NX) % NX; break;
            case 3: snake[0].y = (snake[0].y + N0) % NY; break;
        }

        // grow snake if head lands on food
        for(int j = 0; j < food_length; j++)
        {
            if(food[j].x == snake[0].x && food[j].y == snake[0].y)
            {
                food[j] = food[food_length - 1];
                food_length--;
                snake_length++;
                snake = realloc(snake, snake_length * sizeof *snake);
                snake[snake_length - 1] = last;
                break;
            }
        }

        // spawn food
        if(time % period == 0)
        {
            const int nx = NX / N0;
            const int ny = NY / N0;
            const int n_cells = nx * ny;
            const int n_empty = n_cells - food_length - snake_length;
            if(n_empty > 0)
            {
                const int crand = rand() % n_empty;

                int * is_empty = malloc(n_cells * sizeof *is_empty);

                for(int c = 0; c < n_cells; c++)
                {
                    is_empty[c] = 1;
                }

                for(int i = 0; i < snake_length; i++)
                {
                    const int x = snake[i].x / N0;
                    const int y = snake[i].y / N0;
                    const int c = x + nx * y;
                    is_empty[c] = 0;
                }

                for(int j = 0; j < food_length; j++)
                {
                    const int x = food[j].x / N0;
                    const int y = food[j].y / N0;
                    const int c = x + nx * y;
                    is_empty[c] = 0;
                }

                int counter = -1;
                int c = 0;

                while(1)
                {
                    if(is_empty[c])
                    {
                        counter++;

                        if(counter == crand)
                        {
                            break;
                        }
                    }

                    c++;
                }

                const int x = (c % nx) * N0;
                const int y = (c / nx) * N0;

                food_length++;
                food = realloc(food, food_length * sizeof *food);
                food[food_length - 1].x = x;
                food[food_length - 1].y = y;
                food[food_length - 1].w = N0;
                food[food_length - 1].h = N0;

                free(is_empty);
            }
        }

        // clears the screen
        SDL_SetRenderDrawColor(rend, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(rend);

        // draw snake
        SDL_SetRenderDrawColor(rend, 255, 255, 255, SDL_ALPHA_OPAQUE);
        for(int i = 0; i < snake_length; i++)
        {
            SDL_RenderFillRect(rend, &snake[i]);
        }

        SDL_RenderCopy(rend, tex, NULL, &snake[0]);

        // draw food
        SDL_SetRenderDrawColor(rend, 0, 255, 0, SDL_ALPHA_OPAQUE);
        for(int j = 0; j < food_length; j++)
        {
            SDL_RenderFillRect(rend, &food[j]);
        }

        // losing condition
        int lose = 0;
        for(int i = 1; i < snake_length; i++)
        {
            if(snake[i].x == snake[0].x && snake[i].y == snake[0].y)
            {
                lose = 1;
                break;
            }
        }

        // triggers the double buffers
        // for multiple rendering
        SDL_RenderPresent(rend);

        if(lose)
        {
            SDL_Delay(5000);
            close = 1;
        }

        // calculates to 60 fps
        SDL_Delay(1000 / FPS);
        last_dir = dir;
        time += 1;
    }

    free(snake);
    free(food);

    // destroy texture
    SDL_DestroyTexture(tex);

    // destroy renderer
    SDL_DestroyRenderer(rend);

    // destroy window
    SDL_DestroyWindow(win);

    // close SDL
    SDL_Quit();

    return 0;
}
